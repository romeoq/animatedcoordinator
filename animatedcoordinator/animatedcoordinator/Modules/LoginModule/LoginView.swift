// Created by Roman Voinitchi on 8/18/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

class LoginView: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onStartAppTap(_ sender: Any) {
        if let sceneDelegate = self.view.window?.windowScene?.delegate as? SceneDelegate {
            sceneDelegate.coordinator.start()
        }
        print("Launch app")
    }
    
    
}

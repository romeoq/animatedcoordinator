// Created by Roman Voinitchi on 8/18/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

class LoginCoordinator: ChildCoordinatorType {
    
    private unowned var window: UIWindow!
    
    private lazy var rootNavigationViewController: UINavigationController = {
        let nc = UINavigationController()
        nc.isNavigationBarHidden = true
        window.rootViewController = nc
        return nc
    }()
    
    var navController: UINavigationController {
        return rootNavigationViewController
    }
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        guard let view = getRootView() else {
            return
        }
        rootNavigationViewController.viewControllers = [view]
    }
    
    func getRootView() -> UIViewController? {
        guard let view = UIStoryboard(name: "LoginView", bundle: nil).instantiateViewController(identifier: "LoginView") as? LoginView else {
            return nil
        }
        return view
    }
    
    func start(with viewController: UIViewController) {
        rootNavigationViewController.viewControllers = [viewController]
    }
    
}

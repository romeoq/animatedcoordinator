// Created by Roman Voinitchi on 8/18/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

class Coordinator: CoordinatorType {
    
    private unowned var window: UIWindow!
    private var flowCoordinator: ChildCoordinatorType?
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        if flowCoordinator is LoginCoordinator {
            showAppFlow()
        } else {
            showLoginFlow()
        }
    }
    
    func showAppFlow() {
        if let currentCoordinator = flowCoordinator {
            let appCoordinator = AppCoordinator(window: self.window)
            guard let nextView = appCoordinator.getRootView() else { return }
            currentCoordinator.navController.pushViewController(nextView, animated: true, completion: {
                self.flowCoordinator = appCoordinator
                appCoordinator.start(with: nextView)
            })
        } else {
            let coordinator = AppCoordinator(window: self.window)
            flowCoordinator = coordinator
            flowCoordinator?.start()
        }
    }
    
    func showLoginFlow() {
        if let currentCoordinator = flowCoordinator {
            let loginCoordinator = LoginCoordinator(window: self.window)
            guard let nextView = loginCoordinator.getRootView() else { return }
            currentCoordinator.navController.pushViewController(nextView, animated: true, completion: {
                self.flowCoordinator = loginCoordinator
                loginCoordinator.start(with: nextView)
            })
        } else {
            let loginCoordinator = LoginCoordinator(window: self.window)
            flowCoordinator = loginCoordinator
            flowCoordinator?.start()
        }
    }
}

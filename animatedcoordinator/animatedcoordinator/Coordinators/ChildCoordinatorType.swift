// Created by Roman Voinitchi on 8/18/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

protocol ChildCoordinatorType {
    var navController: UINavigationController { get }
    
    func start()
    func getRootView() -> UIViewController?
    func start(with viewController: UIViewController)
}

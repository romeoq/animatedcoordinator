// Created by Roman Voinitchi on 8/18/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import Foundation

protocol CoordinatorType: class {
    func start()
}

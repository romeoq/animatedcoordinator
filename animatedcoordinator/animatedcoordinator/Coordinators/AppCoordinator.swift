// Created by Roman Voinitchi on 8/18/20
// Copyright © 2020 Roman Voinitchi. All rights reserved.


import UIKit

class AppCoordinator: ChildCoordinatorType {
    
    private unowned var window: UIWindow!
    
    private lazy var rootNavigationViewController: UINavigationController = {
        let nc = UINavigationController()
        nc.isNavigationBarHidden = true
        window.rootViewController = nc
        return nc
    }()
    
    var navController: UINavigationController {
        return rootNavigationViewController
    }
    
    init(window: UIWindow) {
        self.window = window
    }
    
    func start() {
        guard let view = getRootView() else {
            return
        }
        rootNavigationViewController.viewControllers = [view]
    }
    
    func getRootView() -> UIViewController? {
        guard let view = UIStoryboard(name: "AppView", bundle: nil).instantiateViewController(identifier: "AppView") as? AppView else {
            return nil
        }
        return view
    }
    
    func start(with viewController: UIViewController) {
        rootNavigationViewController.viewControllers = [viewController]
    }
    
}
